# Flags
LATEX_FLAGS := -pdf -lualatex -cd -silent

# Directories
RULES = werewolf_llnux_game_rules
RULES_TEX = latex/$(RULES).tex
RULES_PDF = latex/build/$(RULES).pdf

# ------------------------------------------------------------------

.PHONY: all $(RULES) open clean clean_aux

all: open

open: $(RULES)
	@xdg-open $(RULES_PDF) 2>/dev/null 1>/dev/null &

$(RULES): $(RULES_PDF)

$(RULES_PDF): $(RULES_TEX)
	@echo -e "\e[1;7;32m[=]\e[27m Compiling $< to $@ ...\e[0m"
	latexmk $(LATEX_FLAGS) $(LATEX_OPT) -outdir=$(PWD)/$(@D) $< $(BASH_POSTPROCESSING)

clean:
	rm -r latex/build/*

clean-aux:
	rm $(shell find latex/build/* -not -type d -not -path '$(RULES_PDF)')
